package tubesoop;
import java.util.*;

class Login extends User {
    private Map<String, String> userCredentials;
    private String password;

    public Login(String email, String password) {
        super(email);
        this.password = password;
        userCredentials = new HashMap<>();
        userCredentials.put("dhealistia13@gmail.com", "teume123");
    }

    public void displayProfile() {
        System.out.println("\nBerhasil Masuk!");
    }

    public boolean login() {
        if (userCredentials.containsKey(getEmail())) {
            String storedPassword = userCredentials.get(getEmail());
            if (storedPassword.equals(password)) {
                return true;
            }
        }
        return false;
    }
}
