package tubesoop;

abstract class User {
    private String email;

    public User(String email) {
        this.email = email;
    }

    public abstract void displayProfile();

    public String getEmail() {
        return email;
    }
}