package tubesoop;
import java.util.*;

class SignUp extends User {
    private String phoneNumber;
    private String password;

    public SignUp(String email, String phoneNumber, String password) {
        super(email);
        this.phoneNumber = phoneNumber;
        this.password = password;
    }

    public void displayProfile() {
        System.out.println("\nBerhasil Daftar Akun!");
    }
}
