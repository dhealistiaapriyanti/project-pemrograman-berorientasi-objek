package tubesoop;
import java.util.*;

public class JadwalKALokal extends JadwalKereta{
    @Override
    public void infoJadwalKereta(){
        Scanner input = new Scanner(System.in);
        System.out.println("Pencarian Jadwal KA Antar Kota");
        System.out.println("Masukkan Stasiun Asal: ");
        String stasiunAsal = input.nextLine();
        System.out.println("Masukkan Stasiun Tujuan: ");
        String stasiunTujuan = input.nextLine();

        System.out.println("Jadwal Kereta dari " + stasiunAsal + " ke " + stasiunTujuan);
        System.out.println("Kereta WALAHAR EXPRES Rp. 4.000: 08.45 - 06.46");
        System.out.println("Kereta WALAHAR EXPRES Rp. 4.000: 07.00 - 08.12");
        System.out.println("Kereta WALAHAR EXPRES Rp. 4.000: 11.20 - 12.38");
        System.out.println("Kereta WALAHAR EXPRES Rp. 4.000: 13.05 - 14.20");
        System.out.println("Kereta WALAHAR EXPRES Rp. 4.000: 18.45 - 20.07");
        System.out.println("Kereta JATILUHUR \tRp. 4.000: 20.14 - 21.19");
    }
}
