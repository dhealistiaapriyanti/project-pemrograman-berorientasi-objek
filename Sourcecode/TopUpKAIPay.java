package tubesoop;
import java.util.Scanner;

public class TopUpKAIPay {
    private static double saldoKAIPay;

    public static double getSaldoKAIPay() {
        return saldoKAIPay;
    }

    public static void setSaldoKAIPay(double saldo) {
        saldoKAIPay = saldo;
    }

    public static void topUp() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Pilih metode pembayaran:");
        System.out.println("1. ATM");
        System.out.println("2. Internet Banking");
        System.out.println("3. Mobile Banking");
        System.out.println("4. SMS Banking");
        System.out.println("5. Gerai Retail");
        System.out.print("Masukkan pilihan metode pembayaran: ");
        int paymentMethod = scanner.nextInt();
        scanner.nextLine(); // Membersihkan newline

        if (paymentMethod == 1) {
            topUpATM();
        } else if (paymentMethod == 2) {
            topUpInternetBanking();
        } else if (paymentMethod == 3) {
            topUpMobileBanking();
        } else if (paymentMethod == 4) {
            topUpSMSBanking();
        } else if (paymentMethod == 5) {
            topUpGeraiRetail();
        } else {
            System.out.println("Metode pembayaran tidak valid.");
        }
    }

    private static void topUpATM() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n=== Top Up KAIPay - ATM ===");
        System.out.print("Masukkan nomor rekening ATM: ");
        String accountNumber = scanner.nextLine();
        System.out.print("Masukkan jumlah top up: ");
        double topUpAmount = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline

        saldoKAIPay += topUpAmount;

        System.out.println("Top up KAIPay sebesar " + topUpAmount + " berhasil. Saldo KAIPay saat ini: " + saldoKAIPay);
    }

    private static void topUpInternetBanking() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n=== Top Up KAIPay - Internet Banking ===");
        System.out.print("Masukkan nomor rekening Internet Banking: ");
        String accountNumber = scanner.nextLine();
        System.out.print("Masukkan jumlah top up: ");
        double topUpAmount = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline

        saldoKAIPay += topUpAmount;

        System.out.println("Top up KAIPay sebesar " + topUpAmount + " berhasil. Saldo KAIPay saat ini: " + saldoKAIPay);
    }

    private static void topUpMobileBanking() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n=== Top Up KAIPay - Mobile Banking ===");
        System.out.print("Masukkan nomor ponsel Mobile Banking: ");
        String phoneNumber = scanner.nextLine();
        System.out.print("Masukkan jumlah top up: ");
        double topUpAmount = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline

        saldoKAIPay += topUpAmount;

        System.out.println("Top up KAIPay sebesar " + topUpAmount + " berhasil. Saldo KAIPay saat ini: " + saldoKAIPay);
    }

    private static void topUpSMSBanking() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n=== Top Up KAIPay - SMS Banking ===");
        System.out.print("Masukkan nomor ponsel untuk SMS Banking: ");
        String phoneNumber = scanner.nextLine();
        System.out.print("Masukkan jumlah top up: ");
        double topUpAmount = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline

        saldoKAIPay += topUpAmount;

        System.out.println("Top up KAIPay sebesar " + topUpAmount + " berhasil. Saldo KAIPay saat ini: " + saldoKAIPay);
    }
    
    private static void topUpGeraiRetail() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\n=== Top Up KAIPay - Gerai Retail ===");
        System.out.print("Masukkan nomor transaksi pembelian di gerai retail: ");
        String transactionNumber = scanner.nextLine();
        System.out.print("Masukkan jumlah top up: ");
        double topUpAmount = scanner.nextDouble();
        scanner.nextLine(); // Membersihkan newline
    
        saldoKAIPay += topUpAmount;
    
        System.out.println("Top up KAIPay sebesar " + topUpAmount + " berhasil. Saldo KAIPay saat ini: " + saldoKAIPay);
    }
}    
