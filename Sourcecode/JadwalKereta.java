package tubesoop;
import java.util.*;

public class JadwalKereta {
    private String statisunAsal;
    private String statisunTujuan;
    
    public void infoJadwalKereta(){
        Scanner input= new Scanner(System.in);

        System.out.println("Cari Jadwal Kereta: ");
        System.out.println("Masukkan Stasiun Asal: ");
        String stasiunAsal = input.nextLine();
        System.out.println("Masukkan Stasiun Tujuan: ");
        String stasiunTujuan = input.nextLine();
        System.out.println("Masukkan Tanggal Keberangkatan: ");

        System.out.println("Jadwal Kereta dari " + stasiunAsal + " ke " + stasiunTujuan);
        System.out.println("Kereta 1: 09.00 - 09.22");
        System.out.println("Kereta 2: 11.10 - 11.32");
        System.out.println("Kereta 3: 14.45 - 15.08");
        System.out.println("Kereta 4: 17.45 - 18.07");
    }
}