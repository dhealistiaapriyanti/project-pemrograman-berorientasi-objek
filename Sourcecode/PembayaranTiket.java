package tubesoop;
import java.util.Scanner;

class PembayaranTiket {
    private double totalHarga;
    private String metodePembayaran; 

    void bayarTiket(double totalHarga, String metodePembayaran) {
        System.out.println("\nPembayaran Tiket");
        System.out.println("Total Harga: " + totalHarga);

        if (metodePembayaran.equals("1")) {
            bayarTunai(totalHarga);
        } else if (metodePembayaran.equals("2")) {
            bayarTransferBank(totalHarga);
        } else if (metodePembayaran.equals("3")) {
            bayarEWallet(totalHarga);
        } else {
            System.out.println("Metode Pembayaran yang Dipilih tidak valid.");
        }
    }

    private void bayarTunai(double totalHarga) {
        System.out.println("\nPembayaran Tunai");
        System.out.println("Total Harga: " + totalHarga);
        System.out.println("Silakan bayar dikasir.");
    }

    private void bayarTransferBank(double totalHarga) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nPembayaran Transfer Bank");
        System.out.println("Total Harga: " + totalHarga);
        System.out.print("Masukkan nomor rekening tujuan: ");
        String nomorRekening = scanner.nextLine();
    }

    private void bayarEWallet(double totalHarga) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nPembayaran E-Wallet");
        System.out.println("Pilih metode e-wallet:");
        System.out.println("1. LinkAja");
        System.out.println("2. OVO");
        System.out.print("Masukkan pilihan: ");
        int metode = scanner.nextInt();
        scanner.nextLine(); 

        if (metode == 1) {
            System.out.println("Anda memilih metode pembayaran LinkAja.");
            System.out.println("Masukkan Nomor Telepon Anda: " + "Total Harga: " + totalHarga);
            System.out.println("Pembayaran berhasil dilakukan.");
            String nomorTelepon = scanner.nextLine();
        } else if (metode == 2) {
            System.out.println("Anda memilih metode pembayaran OVO.");
            System.out.print("Masukkan Nomor Telepon Anda: " + "Total Harga: " + totalHarga);
            System.out.println("Pembayaran berhasil dilakukan.");
            String nomoTelepon = scanner.nextLine();
        } else {
            System.out.println("Pilihan metode pembayaran tidak valid.");
        }
    }
}
