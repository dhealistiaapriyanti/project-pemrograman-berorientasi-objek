package tubesoop;
import java.util.*;

public class JadwalKAAntarKota extends JadwalKereta{
    @Override
    public void infoJadwalKereta(){
        Scanner input = new Scanner(System.in);
        System.out.println("Pencarian Jadwal KA Antar Kota");
        System.out.print("Masukkan Stasiun Asal: ");
        String stasiunAsal = input.nextLine();
        System.out.print("Masukkan Stasiun Tujuan: ");
        String stasiunTujuan = input.nextLine();

        System.out.println("Jadwal Kereta dari " + stasiunAsal + " ke " + stasiunTujuan);
        System.out.println("Kereta ARGO PARAHYANGAN TAMBAHAN Rp. 250.000: 08.45 - 11.21");
        System.out.println("Kereta ARGO PARAHYANGAN TAMBAHAN Rp. 150.000: 09.35 - 12.03");
        System.out.println("Kereta ARGO PARAHYANGAN TAMBAHAN Rp. 250.000: 09.40 - 12.58");
        System.out.println("Kereta CIKURAY                   Rp. 45.000: 11.10 - 13.38");
        System.out.println("Kereta ARGO PARAHYANGAN TAMBAHAN Rp. 150.000: 18.15 - 20.24");
    }
}
