package tubesoop;
import java.util.*;

public class PemesananTiket {
    protected String nama;
    protected String idPenumpang;
    protected String jenisKereta;
    protected String stasiunAsal;
    protected String stasiunTujuan;
    protected String waktuBerangkat;
    protected String waktuTiba;
    protected String tanggalBerangkat;
    protected int jumlahPenumpang;
    protected double harga;
    protected double totalHarga;
    
    public void inputPenumpang(){
        Scanner input = new Scanner(System.in);
        System.out.println("\n---- Pemesanan Tiket KA ----");
        System.out.println("----------------------------");
        System.out.print("Nama Penumpang: ");
        nama = input.nextLine();
        System.out.print("Id Penumpang: ");
        idPenumpang = input.nextLine();
        System.out.print("Jenis Kereta (Ekonomi/Bisnis/Eksekutif): ");
        jenisKereta = input.nextLine();
        System.out.print("Stasiun Asal: ");
        stasiunAsal = input.nextLine();
        System.out.print("Stasiun Tujuan: ");
        stasiunTujuan = input.nextLine();
        System.out.print("Tanggal Berangkat(DD/MM/YY): ");
        tanggalBerangkat = input.nextLine();
        System.out.print("Waktu Keberangkatan: ");
        waktuBerangkat = input.nextLine();
        System.out.print("Waktu tiba: ");
        waktuTiba = input.nextLine();
        System.out.print("Jumlah Penumpang");
        jumlahPenumpang = input.nextInt();

        hitungTotalHarga();
    }

    public void hitungTotalHarga(){
        totalHarga = jumlahPenumpang * harga;
    }

    public void tampilkanPemesanan(){
        System.out.println("Pesanan Anda!");
        System.out.println("Nama Penumpang: " + nama);
        System.out.println("ID Penumpang: " + idPenumpang);
        System.out.println("Jenis Kereta: " + jenisKereta);
        System.out.println("Stasiun Asal: " + stasiunAsal);
        System.out.println("Satisun Tujuan: " + stasiunTujuan); 
        System.out.println("Tanggal Berangkat(DD/MM/YYYY): " + tanggalBerangkat);
        System.out.println("Waktu Berangkatan: " + waktuBerangkat);
        System.out.println("Waktu Tiba: "  + waktuTiba);
        System.out.println("Harga: " + harga);
        System.out.println("Total Harga: " + totalHarga);
    }

    public double getTotalHarga(){
        return totalHarga;
    }
}