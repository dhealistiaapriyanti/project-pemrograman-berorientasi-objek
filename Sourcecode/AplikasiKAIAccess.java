package tubesoop;

import java.util.*;

public class AplikasiKAIAccess {
    private static String loggedInEmail;
    private static Map<String, User> users;
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean isLoggedIn = false;
        InformasiKRL informasiKRL = new InformasiKRL();
        users = new HashMap<>();

        while(true) {
            System.out.println("\nSelamat Datang di KAI Access!");
            System.out.println("\nBergabunglah untuk keuntungan yang lebih banyak");
            System.out.println("Tidak memiliki akun? Daftar Sekarang.");
            System.out.println("1. Daftar Akun");
            System.out.println("2. Masuk");
            System.out.println("3. Keluar");
            System.out.print("Pilihan Menu: ");
            int pilih = scanner.nextInt();
            scanner.nextLine();
            
            if(pilih == 1) {
                System.out.println("\n----- Daftar Akun -----");
                System.out.print("Email: ");
                String email = scanner.next();
                System.out.print("Nomor telepon: ");
                String phoneNumber = scanner.next();
                System.out.print("Kata Sandi: ");
                String password = scanner.next();

                SignUp signUp = new SignUp(email, phoneNumber, password);
                signUp.displayProfile();
            } else if(pilih == 2) {
                System.out.println("\n------ Masuk ------");
                System.out.print("Email: ");
                String email = scanner.next();
                System.out.print("Kata Sandi: ");
                String password = scanner.next();
                Login login = new Login(email, password);
                login.displayProfile();

                if(login.login()) {
                    while(true) {
                    System.out.println("\n========== KAI Access ==========="); 
                    System.out.println("---------------------------------");
                    System.out.println("1. Jadwal Kereta Api");
                    System.out.println("2. Jenis Kelas Kereta Api");
                    System.out.println("3. Pesan Tiket KA Lokal");
                    System.out.println("4. Pesan Tiket KA Antar Kota");
                    System.out.println("5. Payment Tiket");
                    System.out.println("6. Informasi KRL");
                    System.out.println("7. Top Up KAIPay");
                    System.out.println("0. Keluar");
                    System.out.print("Pilih Menu: ");
                    pilih = scanner.nextInt();
                    scanner.nextLine(); // Membuang new line

                        switch (pilih) {
                            case 1:
                                // Pencarian Jadwal Kereta KA Antar Kota
                                System.out.print("\nJadwal Kereta(KA Antar Kota/KA Lokal): ");
                                int jenisKereta = scanner.nextInt();
                                if(jenisKereta == 1) {
                                    System.out.println("\n------- Jadwal Kereta KA Antar Kota -------");
                                    JadwalKAAntarKota jadwalAK = new JadwalKAAntarKota();
                                    jadwalAK.infoJadwalKereta();
                                } else if(jenisKereta == 2) {
                                    System.out.println("\n------- Jadwal KA Lokal -------");
                                    JadwalKALokal jadwalLK = new JadwalKALokal();
                                    jadwalLK.infoJadwalKereta(); 
                                } else {
                                    System.out.println("Pilihan jenis kereta tidak valid!");
                                }
                                break;
                            case 2:
                                // Lihat Jenis Kereta
                                JenisKelas jenisKelas = new JenisKelas();
                                jenisKelas.displayJenisKelas();
                                break;
                            case 3:
                                // Pesan Tiket KA Lokal
                                TiketKAantarkota pemesananTiketKAAntarKota = new TiketKAantarkota();
                                pemesananTiketKAAntarKota.inputPenumpang();
                                pemesananTiketKAAntarKota.tampilkanPemesanan();
                                break;
                            case 4:
                                // Pesan Tiket KA Lokal
                                TiketKAlokal pemesananTiketKALokal = new TiketKAlokal();
                                pemesananTiketKALokal.inputPenumpang();
                                pemesananTiketKALokal.tampilkanPemesanan();
                            case 5:
                                // Bayar Tiket
                                System.out.println("\n------- Paymnent -------");
                                System.out.print("\nBayar Tiket (KA Antar Kota/Ka Lokal): ");
                                int pilTiketKA = scanner.nextInt();
                                if(pilTiketKA == 1) {
                                    System.out.println("\nSilakan Lakukan Pembayaran Tiket KA Antar Kota");
                                } else if(pilTiketKA == 2){
                                    System.out.println("\nSilakan Lakukan Pembayaran Tiket KA Lokal");
                                } else {
                                    System.out.println("Pilihan tidak valid! Coba lagi.");
                                }

                                System.out.print("Masukkan total harga tiket: ");
                                double totalHarga = scanner.nextDouble();

                                System.out.println("\nMetode Pembayaran:");
                                System.out.println("1. Tunai");
                                System.out.println("2. Transfer Bank");
                                System.out.println("3. E-Wallet");
                                System.out.print("Masukkan pilihan: ");
                                String metodePembayaran = scanner.nextLine();
                                PembayaranTiket pembayaranTiket = new PembayaranTiket();
                                pembayaranTiket.bayarTiket(totalHarga, metodePembayaran);
                                break;
                            case 6:
                                // Informasi2 KRL
                                informasiKRL.tampilkanInformasiKRL();
                            case 7:
                                //Top Up KAIPay
                                System.out.println("\n------- KAIPay -------");
                                TopUpKAIPay bayar = new TopUpKAIPay();
                                bayar.topUp(); 
                                break;
                            case 0:
                                // Keluar
                                System.out.println("\nTerima kasih telah menggunakan Aplikasi KAI Access.");
                                System.exit(0);
                                break;
                            default:
                                System.out.println("Menu tidak valid. Silakan pilih menu yang tersedia.");
                        }

                        if(pilih == 0) {
                            break;
                        }
                    }
                } else {
                    System.out.println("Email atau Kata Sandi salah. Silakan Ulangi.");
                }
            } else if (pilih == 3) {
                System.out.println("Berhasil keluar!.\nTerima kasih telah menggunakan aplikasi KAI Access");
                break;
            } else {
                System.out.println("Pilihan tidak valid!");
            }
        }
    }
}
    
        
