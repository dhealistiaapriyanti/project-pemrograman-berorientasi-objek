package tubesoop;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InformasiKRL {
    private String[] daftarStasiun = {"Cikarang", "Bekasi", "Jakarta", "Depok", "Bogor"};
    private double[] daftarHarga = {10000.0, 15000.0, 20000.0, 12000.0, 18000.0};
    private String[] daftarWaktu = {"08:00", "10:00", "13:15", "15:45", "18:20"};
    private String[] daftarTanggal = {"20-05-2023", "20-05-2023", "20-04-2023", "20-05-2023", "20-03-2023"};

    public void tampilkanInformasiKRL(){
        System.out.println("\nInformasi Jadwal KRL");
        System.out.println("--------------------");
        System.out.println("Daftar Stasiun: ");
        for(int i = 0; i < daftarStasiun.length; i++){
            System.out.println((i + 1) + ". " +daftarStasiun[i]);
        }
        System.out.println("--------------------");
        System.out.println("Daftar Harga dan Jadwal Keberangkatan: ");
        for(int i = 0; i < daftarHarga.length; i++){
            System.out.println("\nKeberangkatan " + daftarStasiun[i] + " - " + daftarStasiun[i + 1]);
            System.out.println("Harga: " + daftarHarga[i]);
            System.out.println("Waktu: " + daftarWaktu[i]);
            System.out.println("Tanggal: " + daftarTanggal[i]);
        }
    }
}
