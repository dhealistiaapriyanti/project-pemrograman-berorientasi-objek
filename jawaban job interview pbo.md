# No. 1
Algoritma yang saya buat hanya program utama yaitu class **'AplikasiKAIAccess'** dan menurut saya algoritma yang digunakan dalam class **'AplikasiKAIAccess'** merupakan algoritma yang paling sulit daripada program class yang lain. 

Program ini menggunakan algoritma pemrograman berbasis objek dengan pendekatan **prosedural**. Algoritma ini melibatkan penggunaan kelas dan objek untuk mengorganisir dan mengelola logika program.

Berikut adalah algoritma umum yang digunakan dalam program:

Membaca input dari pengguna menggunakan objek Scanner.
Menjalankan loop utama untuk memastikan pengguna sudah masuk atau mendaftar sebelum mengakses fitur-fitur aplikasi.
Dalam loop utama:
1. Menampilkan menu pilihan untuk mendaftar akun, masuk, atau keluar.
2. Membaca pilihan pengguna.
3. Melakukan tindakan sesuai dengan pilihan pengguna:
4. Jika pengguna memilih untuk mendaftar akun, memanggil metode signUp() dari kelas SignUp.
5. Jika pengguna memilih untuk masuk, meminta email dan password, lalu memanggil metode login() dari kelas Login untuk memverifikasi kredensial pengguna.
6. Jika pengguna memilih untuk keluar, menampilkan pesan terima kasih dan keluar dari loop utama.
7. Jika pengguna memilih pilihan yang tidak valid, menampilkan pesan kesalahan.
8. Jika masuk berhasil, mengubah nilai variabel isLoggedIn menjadi true dan keluar dari loop utama.
9. Setelah pengguna berhasil masuk, menampilkan menu utama dengan daftar fitur yang tersedia.
10. Menjalankan loop menu utama selama pengguna aktif dalam aplikasi:
- Membaca pilihan pengguna dari menu.
- Melakukan tindakan sesuai dengan pilihan pengguna:
1) Menampilkan jadwal kereta api antar kota atau jadwal KA lokal tergantung pada pilihan pengguna.
2) Menampilkan jenis kelas kereta.
3) Memesan tiket KA antar kota atau KA lokal tergantung pada pilihan pengguna.
4) Melakukan pembayaran tiket dengan meminta total harga dan metode pembayaran.
5) Menampilkan informasi KRL.
6) Melakukan top up KAIPay.
7) Keluar dari aplikasi jika pengguna memilih pilihan 0.
8) Menampilkan pesan kesalahan jika pilihan tidak valid.
9) Setelah pengguna keluar dari aplikasi, menampilkan pesan terima kasih dan menutup objek Scanner.

**Pseudeucode**

Judul: KAIAccessApp
```
Deklarasi: 
Procedure main():
isLoggedIn <- false
email, password: String
informasiKRL <- new InformasiKRL()
```
```
Implementasi: 

    While not true:
        Display "Selamat Datang di KAI Access!
        Display "Bergabunglah untuk keuntungan yang lebih banyak"
        Display "Tidak memiliki akun? Daftar Sekarang"
        Display "1. Daftar Akun"
        Display "2. Masuk"
        Display "0. Keluar"
        Input pilih

        if pilih == 1
            Display ----- Daftar Akun -----
            Display Email
            Display Nomor Telepon
            Display Kata Sandi
        else if pilih == 2
            Display ------ Masuk ------
            Display Email
            Display Kata Sandi
            if login.login
            While is true
            Display "======= KAI Access ======="
            Display "--------------------------"
            Display "1. Jadwal Kereta Api"
            Display "2. Jenis Kelas Kereta Api"
            Display "3. Pesan Tiket KA Lokal"
            Display "4. Pesan Tiket KA Antar Kota"
            Display "5. Payment Tiket"
            Display "6. Informasi KRL"
            Display "7. Top Up KAIPay"
            Display "0. Keluar"
            Input pilih
                Switch pilih:
                    Case 1:
                        Display "Jadwal Kereta(KA Antar Kota/KA Lokal): "
                        Input jenisKereta
                        If jenisKereta == 1:
                            Display "Jadwal Kereta KA Antar Kota"
                            jadwalAK <- new JadwalKAAntarKota()
                            jadwalAK.infoJadwalKereta()
                        Else If jenisKereta == 2:
                            Display "Jadwal KA Lokal"
                            jadwalLK <- new JadwalKALokal()
                            jadwalLK.infoJadwalKereta()
                        Else:
                            Display "Pilihan jenis kereta tidak valid!"
                    Case 2:
                        jenisKelas <- new JenisKelas()
                        jenisKelas.displayJenisKelas()
                    Case 3:
                        pemesananTiketKAAntarKota <- new TiketKAantarkota()
                        pemesananTiketKAAntarKota.inputPenumpang()
                        pemesananTiketKAAntarKota.tampilkanPemesanan()
                    Case 4:
                        pemesananTiketKALokal <- new TiketKAlokal()
                        pemesananTiketKALokal.inputPenumpang()
                        pemesananTiketKALokal.tampilkanPemesanan()
                    Case 5:
                        Display "------- Pembayaran Tiket ------"
                        Display "Bayar Tiket (KA Antar Kota/Ka Lokal)"
                        Input pilTiketKA
                        If pilTiketKA == 1:
                            Display "Silakan Lakukan Pembayaran Tiket KA Antar Kota"
                        Else If pilTiketKA == 2:
                            Display "Silakan Lakukan Pembayaran Tiket KA Lokal"
                        Else:
                            Display "Pilihan tidak valid! Coba lagi."

                        Input totalHarga
                        Input metodePembayaran
                        pembayaranTiket <- new PembayaranTiket
                        pembayaranTiket.bayarTiket(totalHarga, metodePembayaran)
                    Case 6:
                        informasiKRL.tampilkanInformasiKRL()
                    Case 7:
                        Display "------- KAIPay -------"
                        bayar <- new TopUpKAIPay()
                        bayar.topUp()
                    Case 0:
                        Display "Terima kasih telah menggunakan Aplikasi KAI Access."
                        Exit Program
                    Default:
                        Display "Menu tidak valid. Silakan pilih menu yang tersedia."
                if pilih == 0
            else 
            Display "Email atau Kata Sandi salah. Silakan ulangi."
        else if pilih == 3
        Display "Berhasil keluar!. Terima kasih telah menggunakan aplikasi KAI Access."
    else if
    Display "Pilihan tidak valid!"
```             

Berikut implementasi programnya
[Class AplikasiKAIAccess](https://gitlab.com/dhealistiaapriyanti/project-pemrograman-berorientasi-objek/-/blob/main/Sourcecode/AplikasiKAIAccess.java)

# No. 2
Program ini menggunakan algoritma pemrograman berbasis objek dengan pendekatan **prosedural**. Algoritma ini melibatkan penggunaan kelas dan objek untuk mengorganisir dan mengelola logika program.

Berikut adalah algoritma umum yang digunakan dalam program:

Membaca input dari pengguna menggunakan objek Scanner.
Menjalankan loop utama untuk memastikan pengguna sudah masuk atau mendaftar sebelum mengakses fitur-fitur aplikasi.
Dalam loop utama:
1. Menampilkan menu pilihan untuk mendaftar akun, masuk, atau keluar.
2. Membaca pilihan pengguna.
3. Melakukan tindakan sesuai dengan pilihan pengguna:
4. Jika pengguna memilih untuk mendaftar akun, memanggil metode signUp() dari kelas SignUp.
5. Jika pengguna memilih untuk masuk, meminta email dan password, lalu memanggil metode login() dari kelas Login untuk memverifikasi kredensial pengguna.
6. Jika pengguna memilih untuk keluar, menampilkan pesan terima kasih dan keluar dari loop utama.
7. Jika pengguna memilih pilihan yang tidak valid, menampilkan pesan kesalahan.
8. Jika masuk berhasil, mengubah nilai variabel isLoggedIn menjadi true dan keluar dari loop utama.
9. Setelah pengguna berhasil masuk, menampilkan menu utama dengan daftar fitur yang tersedia.
10. Menjalankan loop menu utama selama pengguna aktif dalam aplikasi:
- Membaca pilihan pengguna dari menu.
- Melakukan tindakan sesuai dengan pilihan pengguna:
1) Menampilkan jadwal kereta api antar kota atau jadwal KA lokal tergantung pada pilihan pengguna.
2) Menampilkan jenis kelas kereta.
3) Memesan tiket KA antar kota atau KA lokal tergantung pada pilihan pengguna.
4) Melakukan pembayaran tiket dengan meminta total harga dan metode pembayaran.
5) Menampilkan informasi KRL.
6) Melakukan top up KAIPay.
7) Keluar dari aplikasi jika pengguna memilih pilihan 0.
8) Menampilkan pesan kesalahan jika pilihan tidak valid.
9) Setelah pengguna keluar dari aplikasi, menampilkan pesan terima kasih dan menutup objek Scanner.

[Class AplikasiKAIAccess](Sourcecode/AplikasiKAIAccess.java)

Program class **'AplikasiKAIAccess'** menggunakan algoritma pemrograman berbasis objek dengan pendekatan **prosedural**. Algoritma ini melibatkan penggunaan kelas dan objek untuk mengorganisir dan mengelola logika program.

# No. 3
**KONSEP DASAR OOP**

**Abstraction**, 
Berfungsi untuk menyembunyikan detail yang kompleks dan tidak perlu dibagi objek, sehingga dapat lebih mudah dipahami dan digunakan. Contonya dalam program yaitu class **'User'**. Class **‘User’** dideklarasikan sebagai class abstrak menggunakan kata kunci **‘abstract’**. Abstraksi memungkinkan definisi class tanpa memberikan implementasi lengkap, sehingga tidak dapat membuat objek langsung dari class tersebut. Selain itu class **‘User’** juga mengimplementasikan enskapsulasi pada atribut **‘email’**. **‘email'** dideklarasikan sebagai variabel pribadi yang berarti hanya dapat diakses di dalam class tersebut. Lalu ada polymorfisme, class **‘User’** memiliki metode abstrak **‘displayProfile()’**. 

**Enscapsulation**, 
Memungkinkan pengendalian akses ke data dan method dalam sebuah objek melalui akses modifier seperti, public, private, dan protected. dengan demikian, dapat membantu dalam menerapkan keamanan serta mencegah manipulasi data yang tidak diinginkan. Contoh dalam program yang menggunakan konsep ini ialah class **'PemesananTiket'** dan class **'InformasiKRL'**

**Polymophism**, 
Polymophism dapat mengelola kode lebih efisien. Konsep ini dapat mengelompokkan objek-objek class turunan dalam struktur data umum seperti array atau koleksi. Contoh dalam program adalah class **'JadwalTiket'** yang memiliki method **‘infoJadwaKereta()’** di-override pada kedua subclass, yaitu **‘JadwalKAAntarKota’** dan **‘JadwalKALokal’** 

**Inheritance**, 
Dengan menggunakan konsep Inheritance dapat menggunakan kembali kode yang ada dari class induk ke class turunan. Dengan menggunakan konsep ini dapat menghindari penulisan ulang kode yang sama. Contoh dalam program adalah class **'TiketKAantarkota'** dan class **'TiketKAlokal'** meruapkan subclass dari class **'PemesanaTiket'** sebagai superclassnya.

# No. 4
Enscapsulation digunakan sebagai pengendalian akses ke data dan method dalam sebuah objek melalui akses modifier seperti, public, private, dan protected. Dengan demikian, dapat membantu dalam menerapkan keamanan serta mencegah manipulasi data yang tidak diinginkan. Dalam proses bisnis aplikasi KAI Access hampir semua class mengimplementasikan enscapsulation, diantaranya:
class **'User'**, class **'SignUp'**, class **'Login'**, class **'JadwalKereta'** class **'PemesananTiket'**, class **'PembayaranTiket'**, class **'InformasiKRL'**, class **'TopUpKAIPay'**  

- Class **'User'**
1. Class **'User'** memiliki satua atribut 'email' bertipe String dan memiliki modifier akses private. Artinya atribut email hanya dapat diakses secara langsung di dalam class User sendiri.
2. Class **'User'** memiliki konstruktor satu parameter email yang digunakan untuk menginisialisasi variabel 'email' saat objek User dibuat. Konstruktor ini nantinya akan dipanggil di class turunan dari class **'User'**.
3. Class **'User'** memiliki metode abstrak displayProfile(). Method ini tidak memiliki implementasi di kelas User dan akan diimplementasikan di class turunan. Methd ini akan menampilkan profil pengguna dengan cara yang spesifik untuk setiap kelas turunan.
4. Class **'User'** juga memiliki metode getEmail() yang mengembalikan nilai dari variabel email. Method ini dapat digunakan untuk mengakses email pengguna dari luar kelas User.
5. Secara keseluruhan, class **'User'** menyediakan dasar umum untuk pengguna dalam aplikasi yang lebih spesifik. Kelas ini dapat diwarisi oleh kelas-kelas turunan yang mengimplementasikan metode abstrak displayProfile() sesuai kebutuhan mereka.

Berikut Sourcode dari program class **'User'**
[Class User](Sourcecode/User.java)

- Class **'SignUp'** memiliki penjelasan sebagai berikut:

1. Class **SignUp** memiliki dua atribut yaitu phoneNumber yang bertipe String dan password yang juga bertipe String. Kedua atribut ini memiliki modifier akses private. Artinya atribut tersebut hanya dapat diakses di class SignUp sendiri.
2.  Class **'SignUp'** memiliki konstruktor yang menerima tiga parameter (email, phoneNumber, dan password) Konstruktor ini memanggil konstruktor dari kelas User menggunakan kata kunci super untuk menginisialisasi atribut email. Selanjutnya, atribut phoneNumber dan password diinisialisasi dengan nilai dari parameter yang diterima.
3. Class **'SignUp'** memiliki dua method yaitu displayProfile() mengimplementasikan method abstrak dari kelas User. Method ini menampilkan profil pengguna yang telah mendaftar dengan mencetak informasi email dan nomor telepon ke layar.

Berikut Sourcode dari program class **'SignUp'**
[Class SignUp](Sourcecode/SignUp.java)

- class **'Login'** memiliki penjelasan sebagai berikut:

1. class **Login** memiliki dua atribut yang pertama userCredentials bertipe Map<String, String > digunakan untuk menyimpan pasangan alamat email pengguna dan kata sandi yang terkait.
Kedua atribut password yang bertipe String dan digunakan untuk menyimpan kata sandi yang akan digunakan dalam proses login dan memiliki modifier private yang artinya atribut tersebut hanya dapat diakses diclass **'Login'** sendiri.
2. Class **'Login'** memiliki konstruktor Login(String email, String password) yang menerima dua parameter, yaitu email dan password. Konstruktor ini memanggil konstruktor dari kelas User menggunakan kata kunci super untuk menginisialisasi atribut email. Selain itu, konstruktor ini juga menginisialisasi atribut userCredentials dengan membuat objek HashMap dan menambahkan satu pasangan nilai awal untuk email dan kata sandi yang terkait.
3. Selanjutnya class **'Login'** memiliki dua Method yang pertama displayProfile()diimplementasikan dari Class User. Method ini berfungsi untuk menampilkan pesan "Berhasil Masuk!" ke layar. Kedua login() method ini memeriksa apakah alamat email yang dimasukkan pengguna ada dalam userCredentials. Jika ada, maka kata sandi yang terkait dengan alamat email tersebut dibandingkan dengan kata sandi yang dimasukkan pengguna. Jika kedua kata sandi cocok, metode ini mengembalikan true, yang menunjukkan login berhasil. Jika tidak, method ini mengembalikan false, yang menunjukkan login gagal.

Berikut Sourcode dari program class **'Login'** [Class Login](Sourcecode/Login.java)

- Class **'JadwalKereta'** memiliki penjelasan sebagai berikut:

1. Class **'JadwalKerta'** memiliki dua atribut yaitu statisunAsal dan stasiunTujuan keduanya bertipe String dan memiliki modifier private yang artinya atribut tersebut hanya dapat diakses oleh class **'JadwalKereta'** itu sendiri.
2. Class **'JadwalKerta'** memiliki method infoJadwalKereta() digunakan untuk menampilkan informasi jadwal kereta. Meto ini meminta pengguna memasukkan stasiun asal dan stasiun tujuan menggunakan objek Scanner. Setelah menerima input dari pengguna, method ini mencetak jadwal kereta dari stasiun asal ke stasiun tujuan ke layar. 

Berikut Sourcode dari program class **'JadwalKereta'** [Class JadwalKereta](Sourcecode/JadwalKereta.java)

- class **'PemesananTiket'** memiliki penejelasan sebagai berikut:

1.	Atribut class **'PemesananTiket'** (nama, idPenumpang, jenisKereta, stasiunAsal, stasiunTujuan, waktuBerangkat, waktuTiba, tanggalBerangkat, jumlahPenumpang, harga, totalHarga) dideklarasikan sebagai protected, yang berarti hanya dapat diakses oleh class itu sendiri dan subclass-nya. Ini membantu dalam menerapkan prinsip enkapsulasi dengan membatasi akses langsung ke atribut.
2.	Terdapat beberapa method publik (inputPenumpang(), hitungTotalHarga(), tampilkanPemesanan(), getTotalHarga()) yang digunakan untuk berinteraksi dengan objek PemesananTiket. Melalui method-methodeini, pengguna dapat memasukkan data penumpang, menghitung total harga, menampilkan detail pemesanan, dan mengambil nilai total harga. Dengan menggunakan metode-metode publik ini, akses ke atribut-atribut class dilakukan secara terkontrol, sehingga menjaga integritas dan keamanan data.

Berikut Sourcode class **'PemesananTiket'** 
[Class PemesananTiket](Sourcecode/PemesananTiket.java)
        
- Class **'PembayaranTiket'** memiliki penjelasan sebagai berikut:
1. Class **'PembayaranTiket'** memiliki dua atribut yaitu totalHarga bertipe double dan metodePembayaran bertipe String keduanya memiliki modifier private, yang artinya hanya dapat diaskes didalam class **'PembayaranTiket'** sendiri.
2. Class **'PembayaranTiket'** juga memiliki beberapa method diantaranya, bayarTiket(double totalHarga, String metodePembayaran) digunakan untuk melakukan pembayran tiket, bayarTunai(double totalHarga) digunakan untuk melakukan pembayran tunai, bayarTransferBank(double totalHarga) digunkana untuk melakukan pembayaran melalui transfer bank, dan yang terakhir bayarEWallet(double totalHarga) digunkan untuk melakukan pembayaran melalui e-wallet.

Berikut Sourcode dari program class **'PembayaranTiket'** [Class PembayaranTiket](Sourcecode/PembayaranTiket.java)


- Class **'InformasiKRL'** memiliki penjelasan sebagai berikut:

1. Atribut class seperti (daftarStasiun, daftarHarga, daftarWaktu, dan daftarTanggal), dideklarasikan sebagai variabel private dan tidak dapat diakses langsung dari luar kelas.
2. Fungsi tampilkanInformasiKRL() digunakan untuk mengakses dan menampilkan informasi KRL.

Berikut Sourcode class **'InformasiKRL'** 
[Class InformasiKRL](Sourcecode/InformasiKRL.java)

# No. 5
Abstraction berfungsi untuk menyembunyikan detail yang kompleks dan tidak perlu dibagi objek, sehingga dapat lebih mudah dipahami dan digunakan. Dalam proses bisnis yang dibuat yaitu aplikasi KAI Access terdapat beberapa class yang menggunakan konsep **Abstraction** diantaranya:

Class **‘User’** dideklarasikan sebagai class abstrak menggunakan kata kunci **‘abstract’**. Abstraksi memungkinkan definisi class tanpa memberikan implementasi lengkap, sehingga tidak dapat membuat objek langsung dari class tersebut. Selain itu class **‘User’** juga mengimplementasikan enskapsulasi pada atribut **‘email’**. **‘email'** dideklarasikan sebagai variabel pribadi yang berarti hanya dapat diakses di dalam class tersebut. Lalu ada polymorfisme, class **‘User’** memiliki metode abstrak **‘displayProfile()’**. Metode ini dideklarasikan tanpa detail implementasi, dan subclass dari class **‘User’** akan memberikan implementasi yang sesuai dengan kebutuhan masing-masing. Inheritance pada class **‘User’** berfungsi sebagai superclass yang diturunkan darinya seperti class **‘SignUp’** dan class **‘Login’**.

Berikut Sourcecode dari program class **'User'** [Class User](Sourcecode/User.java)


# No. 6
Inheritance dapat menggunakan kembali kode yang ada dari class induk ke class turunan, dalam konteks keamanan dengan menggunakan inheritance dapat menghindari duplikasi kode dan memastikan bahwa prinsip keamanan yang sama diterapkan secara konsisten di program aplikasinya. Dilakukan dengan cara membagi fungsi keamanan menjadi bagian-bagian yang terpisah. Terdapat class yang mengelola otorisasi,class yang mengelola enkripsi, class yang mengelola validasi input, dan sebagainya. Dengan memisahkan fungsi keamanan ke dalam class yang terpisah, dapat meningkatkan modularitas dan pemeliharaan kode, serta memudahkan pengembangan, pembaruan, dan pengujian komponen keamanan. Selanjutnya memiliki class dasar yang menangani penanganan kesalahan keamanan umum, seperti penanganan kesalahan autentikasi atau penanganan penolakan lalu akses Objek turunan dapat menambahkan penanganan kesalahan yang spesifik untuk kasus-kasus yang lebih khusus. Ini membantu dalam pengelolaan kesalahan secara terpusat dan menyederhanakan proses penanganan kesalahan keamanan 

Polymophism dapat mengelola kode lebih efisien. Konsep ini dapat mengelompokkan objek-objek class turunan dalam struktur data umum seperti array atau koleksi. Dalam proses bisnis yang dibuat yaitu aplikasi KAI Access terdapat beberapa class yang menggunakan konsep **Inheritance** dan **'Polymophism'** diantaranya:

1. class **'User'**

Inheritance:
- Class **‘SignUp’** dan **‘Login’** merupakan subclass dari class **‘User'**. 
- Class **‘SignUp’** dan **‘Login’** menggunakan keyword **‘extends’** untuk mewarisi atribut dan method dari class **‘User'**.
- Class **‘SignUp’** dan **‘Login’** mewarisi **‘email’** dan method **‘getEmail()’** dari class **‘user’**
- Dengan inheritance class **‘SignUp’** dan **‘Login’** dapat menggunakan dan mengakses atribut dan method yang sudahdidefinisikan di class **‘User’**.
- Melalui inheritance, class SignUp dan Login dapat memanfaatkan fitur dan fungsionalitas yang sudah ada di class User tanpa perlu mengulang kode yang sama.

Polymorphism:
- Method **‘displayProfile()'** dideklarasikan sebagai method abstrak** ‘User’** dengan keywords **‘abstract’**. 
- SignUp dan Login mengimplementasikan metode **'displayProfile()'** dengan cara yang berbeda sesuai dengan tipe pengguna (sign up atau login).
- Polimorfisme memungkinkan kita untuk menggunakan metode **'displayProfile()'** untuk menampilkan profil pengguna dengan cara yang sesuai untuk masing-masing tipe pengguna (sign up atau login).

Berikut Sourcecode dari program class **'User'** [Class User](Sourcecode/User.java)

2. class **'JadwalKereta'**

Inheritance:

- Class **‘JadwalKAAntarKota’** dan class **‘JadwalKALokal’** merupakan subclass dari class **‘JadwalKereta'**
- Subclass mewarisi semua atribut dan method dari superclass. Dalam hal ini, subclass **‘JadwalKAAntarKota'** dan **‘JadwalKALokal’** mewarisi atribut dan method dari superclass **‘JadwalKereta’**.

Polymorphism:

- Terdapat method **‘infoJadwaKereta()’** yang di-override pada kedua subclass, yaitu **‘JadwalKAAntarKota’** dan **‘JadwalKALokal’**
- Method **‘infoJadwalKereta()’** padad superclass **‘JadwalKereta’** dideklarasikan dengan kata kunci **‘public’** dan tanpa implementasi 
- Subclass **‘JadwalKAAntarKota’** dan **‘JadwalKALokal’** mengimplementasikan method **‘infoJadwalKereta()’** dengan implementasi yang sesuai untuk subclassnya
- Polymorphism memungkinkan objek dari subclass (‘JadwalKAAntarKota’ dan 'JadwalKALokal’) dapat dianggap sebagai objek superclass (‘JadwalKereta')
- Pemanggilan method **‘infoJadwalKereta’** pada objek subclass akan menjalankan implementasi yang sesuai dengan subclass tersebut.

Berikut Sourcode dari program class **'JadwalKereta'** [Class JadwalKereta](Sourcecode/JadwalKereta.java)

# No. 7
Dalam program proses bisnis KAI Access ada beberapa use case yang saya buatkan programnya diantaranya sebagai berikut:
1. use case registrasi, diimplementasikan dengan membuat class user memiliki class turunan yaitu class signUp dan class Login ketika class tersebut berfungsi untuk pengguna dapat membuat akun dengan cara mnedaftar selanjutnya masuk ke aplikasi
2. use case login, diimplementasikan dengan membuat class Login untuk masuk ke dalam aplikasi
3. use case  Lihat jadwal kereta saya buat dengan membuat class JadwalKereta yang memiliki class turunan yaitu class JadwalKAAntarKota dan class JadwalKALokal berfungsi untuk melihat jadwal kereta yang tersedia di dalam aplikasi.
4. use case Pilih jenis kereta diimplementasikan dengan membuat class JadwalKereta yang didalamnya terdapat pilihan jenis kereta yang tersedia dalam aplikasi.
5. use case pilih kelas kereta diimplementasikan dengan membuat class KelasKereta yang berfungsi untuk menampilkan jenis kelas kereta seperti, ekonomi, eksekutif, dan bisnis.
6. use case pesan tiket, use case pilih tanggal keberangkatan, use case stasiun asal, use case stasiun tujuan, use case jumlah penumpang, diimplementasikan dengan membuat class PemesananTiket yang didalamnya akan menampilkan pilihan tanggal keberangkatan, stasiun asal, stasiun tujuan, lalu dapat menambahkan jumlah penumpang.
7. use case Bayar tiket, use case metode pembayaran diimplementasikan dengan membuat class PembayaranTiket yang didalamnya terdapat cara untuk membayar tiket yang telah dipesan beserta metode pembayarannya.
8. use case informasiKRL, use case posisi kereta, use case jadwal kereta, use case rute kereta, use case jalur kereta, use case tarif kereta implementasi dengan membuat class InformasiKRL yang berfungsi untuk menampilkan poisis kereta, jadeawl kereta, rute kereta, jalur kereta dan tarif kereta.
9. use casr Top Up KAIPay diimplementasikan dengan cara membuat class TopUpKAIPay yang didalamnya terdapat pilihan metode pembayaran serta pilihan untuk bayarnya.

# No. 8
Pada program proses bisnis Aplikasi KAI Access setelah saya identifikasi terdapat 72 use case yang terdiri dari, 48 use case User dan 24 use case administrator. Namun, tidak semua use case saya buatkan programnya dan hanya beberapa use case yang saya buatkan programnya seperti use case user registarsi, login, lihat jadwal kereta, piilih jenis kereta, pilih kelas kereta, pesan tiket, pilih tanggal, pilih statsiun asal, pilih statisun tujuan, pilih jumlah penumpang, bayar tiket, metode pembayaran, tambahkan tiket, informasi KRL, posisi kereta, jadwal kereta, rute kereta, jalur kereta, tarif kereta, top up KAIPay, dan tiket.

Berikut use case table nya

![](ss/Use_case_KAIAccess1.png)
![](ss/Use_case_KAIAccess2.png)
![](ss/Use_case_KAIAccess3.png)
![](ss/Use_case_KAIAccess4.png)
![](ss/Use_case_KAIAccess5.png)
![](ss/Use_case_KAIAccess6.png)

Class diagram dari seluruh prorgam

![](ss/KAIAccess.drawio.png)

# No. 9
Berikut link youtube proses bisnis aplikasi KAI Access
https://youtu.be/rVZnnJl1oCU

# No. 10
Berikut link youtube proses bisnis aplikasi KAI Access
https://youtu.be/rVZnnJl1oCU
